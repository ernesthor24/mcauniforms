import React, { Component } from 'react';
import './Components.css'

const pitchersample = '/images/pitchersample.png';

class Pitcher extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <div className="pitcher-content">
          <h1 className="division-header"> Grab your school needs </h1>
            <div className="col-md-4 pitcher-uni">
              <img src={pitchersample} className="pitcher-img"/>
              <h2 className="sub-header center"> School Uniforms </h2>
            </div>
            <div className="col-md-4 pitcher-meds">
              <img src={pitchersample} className="pitcher-img"/>
              <h2 className="sub-header center"> Medical Supplies </h2>
            </div>
            <div className="col-md-4 pitcher-acc">
              <img src={pitchersample} className="pitcher-img"/>
              <h2 className="sub-header center"> Accesories </h2>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Pitcher
